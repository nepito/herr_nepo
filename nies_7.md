## Filosofía de juego
### Sustituciones
En general, tratamos de hacer los cambios por dos motivos: para refrescar o para
corregir. 

Los cambios para refrescar son cambios de rutina. Consideramos 
estos cambios para aprovechar las reglas. Desde el primer día hemos hablado de 
ellos.

Los cambios para corregir sirven para mejorar el funcionamiento del equipo. El mal 
funcionamiento puede deberse a confusiones de los jugadores, al llevar a 
cabo las indicaciones, o por un planteamiento incorrecto. En general, los 
jugadores volverán a entrar; para eso deberemos de corregir y platicar que es lo
que no está saliendo bien. En la medida de lo posible, la dirección técnica 
hablará con el jugador que entrará para decir cuál es la corrección que 
esperamos. Con ese ejemplo, el juagador que salió, podrá ver qué se espera de 
él. Así, esperamos que cuando el jugador regrese a la banca se le explique con 
ejemplos qué espera el equipo de él.

Siempre el equipo es lo más importante. Trataremos siempre de priorizar las 
necesidades del equipo sobre las necesidades personales.

## Decálogo
- El primer criterio para ser titular es ir a jugar. Si faltaste al partido pasado no tienes derecho
  a reclamar ser titular.
- Preferimos salir jugando por abajo que a balonazos. Podemos salir a balón largo, pero siempre
  haremos un pase por el suelo primero.
- El portero siempre la puede reventar. Si así lo hace esa debe de ser una decisión respetada. Si él
  la falla ya no tenemos otra persona para resolver su error.
- Podemos fallar todo lo que queramos, pero debemos fallar con intención.
- Las formas son más importantes que los resultados. Preferimos jugar bien sobre cualquier otra
  cosa.
- Que todos los jugadores toquen la pelota es lo que entendemos con jugar bien. Es por eso que
  preferimos salir jugando a salir a balonazos. No importa que caiga el gol, preferimos salir
  jugando.
- Los tiros libres no los volamos. Volar un tiro libre amerita a que el jugador salga de cambio.
  Volar el balón no permite que el resto del equipo juegue. Todo se vale, pero volar el balón es no
  respetar el esfuerzo de tus compañeros.
- Todos bajamos. Cada vez que perdemos el balón todos los jugadores bajamos a nuestro campo.
- Todos los jugadores van a jugar. Todos deberían de jugar más o menos el mismo tiempo. Para
  lograrlo todos deberemos de salir de cambio.
- Todos los jugadores pagan arbitraje.

## Sistema de juego
Sistema 1-1-3-2

### Introducción
El sistema de juego 1-1-3-2 en fútbol 7 es una formación que busca equilibrar la defensa y el ataque, proporcionando solidez en el medio campo y opciones ofensivas versátiles. A continuación se describen las posiciones y funciones de cada jugador en este esquema.

### Posiciones y Funciones

#### 1. Portero (1)
- **Función principal**: Defensa del arco.
- **Tareas específicas**:
  - Parar los disparos del equipo contrario.
  - Organizar la defensa.
  - Iniciar las jugadas desde el fondo con pases precisos.

#### 2. Defensa Central (1)
- **Función principal**: Protección central.
- **Tareas específicas**:
  - Marcar al delantero centro del equipo contrario.
  - Interceptar pases y despejar balones peligrosos.
  - Apoyar al portero en situaciones de peligro.

#### 3. Mediocampistas (3)
##### Medio Centro Defensivo
- **Función principal**: Equilibrio defensivo y transición.
- **Tareas específicas**:
  - Recuperar balones en el medio campo.
  - Apoyar a la defensa en tareas de cobertura.
  - Iniciar la construcción de juego desde la defensa hacia el ataque.

##### Mediocampista Izquierdo
- **Función principal**: Control de banda izquierda.
- **Tareas específicas**:
  - Desbordar por la banda.
  - Proveer centros al área.
  - Ayudar en tareas defensivas cuando sea necesario.

##### Mediocampista Derecho
- **Función principal**: Control de banda derecha.
- **Tareas específicas**:
  - Desbordar por la banda.
  - Proveer centros al área.
  - Ayudar en tareas defensivas cuando sea necesario.

#### 5. Delanteros (2)
##### Delantero Izquierdo
- **Función principal**: Ataque por la izquierda.
- **Tareas específicas**:
  - Buscar oportunidades de gol.
  - Desbordar y realizar tiros a portería.
  - Presionar la salida del rival.

##### Delantero Derecho
- **Función principal**: Ataque por la derecha.
- **Tareas específicas**:
  - Buscar oportunidades de gol.
  - Desbordar y realizar tiros a portería.
  - Presionar la salida del rival.

## Principios de juego
El campo se divide en tres carriles (izquierdo, central y derecho) y en tres tercios (defensivo, medio y ofensivo).

### División del Campo
- **Carriles**: Izquierdo, Central, Derecho.
- **Tercios**: Defensivo, Medio, Ofensivo.

### Principios ofensivos
- **Transiciones rápidas**: Aprovechar la recuperación de balón para lanzar contraataques rápidos.
- **Juego por las bandas**: Utilizar a los mediocampistas laterales para desbordar y generar oportunidades de gol.
- **Posesión de balón**: Mantener la posesión para desgastar al rival y crear espacios.
- **Amplitud de juego**: Extender el juego a lo ancho del campo para abrir la defensa contraria. Los mediocampistas laterales deben pegarse a la línea de banda y los delanteros deben moverse hacia los extremos para crear espacios en el centro del campo.

#### Fase de ataque

##### Balones Largos
**Principio de Transición Rápida**:Esta estrategia maximiza las oportunidades de ganar la posesión en áreas avanzadas del campo y permite una transición rápida hacia situaciones de ataque peligrosas.
En la fase de ataque, cuando se opta por jugar con balones largos, se seguirán las siguientes directrices:

###### Delanteros (2)
- **Función principal**: Buscar y recibir los pases largos.
- **Tareas específicas**:
  - Desmarcarse y moverse hacia el espacio libre para recibir el balón.
  - Utilizar su velocidad y capacidad de anticipación para ganar la posición.
  - Intentar controlar el balón y avanzar hacia la portería o asistir a un compañero.

###### Mediocampistas (3)
- **Función principal**: Ganar los segundos balones.
- **Tareas específicas**:
  - Anticipar el lugar donde caerá el balón después de un duelo aéreo o un despeje.
  - Realizar movimientos coordinados para cubrir las zonas alrededor del área donde cae el balón.
  - Recuperar la posesión y continuar la jugada ofensiva, buscando opciones de pase hacia adelante o disparos a portería.

###### Coordinación y Movimientos
- **Delanteros**: 
  - Uno de los delanteros se posiciona más adelante para recibir el balón largo, mientras el otro busca espacios para un pase en profundidad.
- **Mediocampistas**:
  - El mediocampista central se posiciona para recoger cualquier balón que caiga en el centro del campo.
  - Los mediocampistas laterales se desplazan hacia el centro y hacia adelante para estar en posición de recoger los segundos balones y ofrecer apoyo inmediato al delantero que reciba el pase largo.


##### Salida limpia a balón controlado

**Principio de Amplitud**: En la fase de salida a balón parado, se intentará jugar con amplitud utilizando a los mediocampistas laterales. A continuación se detallan los movimientos y roles específicos:

###### Mediocampistas Laterales (2)
- **Función principal**: Extender el juego hacia las bandas y proporcionar balones al espacio.
- **Tareas específicas**:
  - Desplazarse hacia las líneas de banda para crear amplitud y abrir la defensa rival.
  - Recibir el balón de la defensa o del medio centro defensivo.
  - Jugar balones al espacio hacia el delantero de su banda.

###### Delanteros (2)
- **Función principal**: Explorar los espacios generados por los balones al espacio y crear opciones de ataque.
  - **Delantero de la Banda**:
    - Moverse hacia los espacios abiertos en la banda.
    - Recibir los balones al espacio de los mediocampistas laterales.
    - Crear jugadas de ataque desde la banda, ya sea centrando o buscando el área.

  - **Delantero del Lado Opuesto**:
    - Explotar el espacio generado por el primer delantero.
    - Hacer movimientos de desmarque hacia el centro y hacia adelante, aprovechando la atención que atrae el primer delantero.
    - Buscar oportunidades de recibir el balón en áreas más centrales y avanzadas.

###### Coordinación y Movimientos
- **Defensa y Medio Centro Defensivo**:
  - Jugar pases precisos hacia los mediocampistas laterales para iniciar la jugada.
  - Mantener una posición de apoyo y estar listos para recuperar el balón en caso de pérdida.

- **Mediocampistas Laterales**:
  - Mantenerse pegados a la línea de banda para maximizar la amplitud.
  - Jugar balones al espacio hacia el delantero de su banda en el momento oportuno.
  - Estar atentos para recibir devoluciones o continuar la jugada en caso de que el delantero decida retrasar el balón.

- **Delanteros**:
  - Coordinar movimientos para no ocupar el mismo espacio y maximizar las opciones de pase.
  - El delantero de la banda busca explotar la banda, mientras que el otro delantero se mueve hacia el espacio generado por el primer delantero, creando opciones en el centro del campo.

Esta estrategia busca aprovechar la amplitud del campo y los movimientos coordinados para crear oportunidades de ataque eficaces y desestabilizar la defensa rival.




### Principios Defensivos

#### Fase de defensa
- **Presión en las bandas**: Concentrar la presión defensiva en los carriles izquierdo y derecho.
  - Los mediocampistas laterales y los delanteros deben presionar activamente al jugador rival que tenga el balón en las bandas.
  - El medio centro defensivo se desplaza hacia el carril donde se encuentra el balón para dar apoyo.
  - El defensa central se mantiene en el carril central, listo para interceptar cualquier pase hacia el centro.

- **Dejar libre la banda opuesta**: La banda opuesta al lugar donde se está ejerciendo la presión se deja relativamente libre.
  - Esta táctica busca forzar al rival a cambiar el juego a través de pases largos y arriesgados.
  - Al dejar libre la banda opuesta, el equipo se asegura de que la mayor cantidad de jugadores estén concentrados en la zona donde está el balón, aumentando las posibilidades de recuperación.


##### Forzar al Rival a Jugar a las Bandas

**Principio de Espera en el Tercio Central**: En la fase defensiva, el equipo se posicionará en el tercio central del campo. El objetivo es evitar que el rival tenga superioridad numérica en la salida, forzarlos a jugar a las bandas o provocar que jueguen balones cortos.


###### Defensa Central (1)
- **Función principal**: Protección del área central.
- **Tareas específicas**:
  - Mantener una posición central para interceptar balones largos.
  - Coordinar con el medio centro defensivo y los mediocampistas laterales para asegurar una defensa compacta.

###### Medio Centro Defensivo (1)
- **Función principal**: Seguir al mediocentro rival.
- **Tareas específicas**:
  - Marcar de cerca al mediocentro rival para evitar que reciba el balón con facilidad.
  - Asegurarse de que el rival no tenga superioridad numérica en la salida del balón.
  - Forzar al rival a jugar hacia las bandas manteniendo presión constante en el centro.

###### Mediocampistas Laterales (2)
- **Función principal**: Control de bandas y apoyo defensivo.
- **Tareas específicas**:
  - Mantener posiciones estrechas al centro para forzar al rival a jugar hacia las bandas.
  - Ayudar en la recuperación del balón y en la presión alta si el rival intenta avanzar por las bandas.
  - Apoyar al medio centro defensivo en caso de que el rival intente atacar por el centro.

###### Delanteros (2)
- **Función principal**: Presión inicial y cobertura de espacios.
- **Tareas específicas**:
  - Presionar a los defensas rivales para dificultar su salida limpia.
  - Estar atentos para interceptar pases y forzar errores.
  - Cubrir espacios en el centro del campo para evitar pases fáciles hacia el mediocentro rival.

###### Coordinación y Movimientos
- **Defensa Central y Medio Centro Defensivo**:
  - Mantener una línea defensiva compacta.
  - El medio centro sigue al mediocentro rival, pero sin desorganizar la línea defensiva.
  - La defensa central se posiciona para interceptar balones largos y apoyar en la cobertura.

- **Mediocampistas Laterales**:
  - Mantenerse estrechos al centro y preparados para apoyar en la defensa de las bandas.
  - Coordinar con los delanteros y el medio centro defensivo para presionar cuando sea necesario.
  - Forzar al rival a jugar hacia las bandas cerrando los espacios centrales.

- **Delanteros**:
  - Iniciar la presión sobre los defensas rivales, buscando dirigir el juego hacia las bandas.
  - Cubrir espacios en el centro para evitar pases fáciles hacia el mediocentro rival.
  - Colaborar con los mediocampistas laterales para forzar el juego hacia los costados.

#### Ejemplo de Situación Defensiva
- **Balón en el Centro del Tercio Defensivo del Rival**:
  - Los delanteros presionan hacia los lados, cerrando el centro y obligando a los defensores rivales a jugar hacia las bandas.
  - El medio centro defensivo se posiciona para interceptar cualquier pase central.
  - Los mediocampistas laterales están listos para presionar a los jugadores de banda del rival cuando reciban el balón.

- **Balón en la Banda Izquierda del Tercio Medio del Rival**:
  - El mediocampista lateral izquierdo presiona al portador del balón.
  - El mediocampista central y el  mediocampista lateral derecho se posicionan para interceptar pases hacia el centro.
  - El delantero izquierdo apoya en la presión y se posiciona para interceptar cualquier intento de pase apoyo a los defensas o portero rival.
  - El delantero derecho recorre por el carril central.

Esta estrategia busca controlar el tercio central del campo, evitar la superioridad numérica del rival en la salida, y forzar el juego hacia las bandas, facilitando la recuperación del balón por parte de nuestro equipo y provocando errores en el rival.



#### Ejemplo de Situación
- **Balón en el Carril Derecho, Tercio Medio**:
  - Los mediocampistas derecho y central presionan al portador del balón.
  - El medio centro defensivo se desplaza hacia el carril derecho para apoyar.
  - El defensa central mantiene su posición en el carril central.
  - El mediocampista izquierdo y el delantero izquierdo se posicionan para interceptar posibles pases cruzados.

- **Balón en el Carril Izquierdo, Tercio Defensivo**:
  - Los mediocampistas izquierdo y central presionan al portador del balón.
  - El medio centro defensivo se desplaza hacia el carril izquierdo para apoyar.
  - El defensa central mantiene su posición en el carril central.
  - El mediocampista derecho y el delantero derecho se posicionan para interceptar posibles pases cruzados.

## Referencias
- [FÚTBOL 7 Táctica | SISTEMA DE JUEGO 1-1-3-2 (Aprende cómo usarlo)](https://youtu.be/C9pJahMo-Jk?si=gy5OAnVHkwc07WA-)
- [Fútbol 7 Táctica | SALIDA DE BALÓN 3-1-2 vs TODOS LOS SISTEMAS](https://youtu.be/I5eIkF8v7ZY?si=yoD5I-1ebef43zyy)
- [5 maneras de SACAR un CORNER en FÚTBOL 7 (abp saque de esquina)](https://youtu.be/lgat5jSueXo?si=LWULIZPwi2FnYmvT)